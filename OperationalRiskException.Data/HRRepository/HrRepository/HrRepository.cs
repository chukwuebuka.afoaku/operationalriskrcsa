﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OperationalRiskException.Data.HRRepository; 
namespace OperationalRiskException.Data.HRRepository.HrRepository
{
    public class HrRepository<TEntity> : IHrRepository<TEntity> where TEntity : class
    {
        private readonly string _connectionString;

        public HrRepository(string conn)
        {
            _connectionString = conn;
        }
        public IEnumerable<TEntity> GetWithRawSql(string query)
        {
            var res = new List<TEntity>();
            try
            {
                using (SqlConnection sql = new SqlConnection(_connectionString))
                {
                    sql.Open();
                    SqlCommand cmd = new SqlCommand(query, sql);
                    var reader = cmd.ExecuteReader();
                    DataTable tbl = new DataTable();
                    tbl.Load(reader);
                    return tbl.Rows.Cast<TEntity>().Select(row => row);
                }

              
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }
    }
}
