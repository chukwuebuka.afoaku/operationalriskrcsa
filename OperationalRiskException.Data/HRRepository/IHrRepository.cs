﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Data.HRRepository
{
    public interface IHrRepository <TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetWithRawSql(string query);

    }
}
