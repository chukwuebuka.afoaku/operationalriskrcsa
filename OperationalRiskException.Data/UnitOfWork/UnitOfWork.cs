﻿using OperationalRiskException.Data.GenericRepository.IRepository;
using OperationalRiskException.Data.GenericRepository.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Data.UnitOfWork
{

    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context = null;
        public Dictionary<Type, object> repositories = new Dictionary<Type, object>();

        public UnitOfWork(DbContext context)
        {
            this._context = context;
        }
       
        public void BeginTransaction()
        {
            _context.Database.BeginTransaction();  
        }

        public void Commit()
        {
            _context.Database.CurrentTransaction.Commit();
        }

        public IRepository<T> Repository<T>() where T : class
        {
            if (repositories.Keys.Contains(typeof(T)) == true)
            {
                return repositories[typeof(T)] as IRepository<T>;
            }

            IRepository<T> repo = new Repository<T>();
            repositories.Add(typeof(T), repo);
            return repo;
        }

        public void RollBack()
        {
            _context.Database.CurrentTransaction.Rollback();
        }

        public async Task<int> SaveChangesAsync()
        {
           return await _context.SaveChangesAsync();
        }
    }
}