﻿namespace OperationalRiskException.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class latest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnswerEntities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionId = c.Int(nullable: false),
                        answerEnum = c.Int(nullable: false),
                        AssessmentRecordId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AssessmentRecords", t => t.AssessmentRecordId, cascadeDelete: true)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId)
                .Index(t => t.AssessmentRecordId);
            
            CreateTable(
                "dbo.AssessmentRecords",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FilePath = c.String(),
                        QuestionCategoryId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        AssessmentSetupId = c.Int(nullable: false),
                        IsAnswered = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AssessmentSetups", t => t.AssessmentSetupId, cascadeDelete: true)
                .ForeignKey("dbo.QuestionCategories", t => t.QuestionCategoryId, cascadeDelete: true)
                .Index(t => t.QuestionCategoryId)
                .Index(t => t.AssessmentSetupId);
            
            CreateTable(
                "dbo.AssessmentSetups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssessmentName = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        QuestionCategories = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QuestionCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionText = c.String(),
                        BatchNo = c.String(),
                        QuestionCategoryId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionCategories", t => t.QuestionCategoryId, cascadeDelete: true)
                .Index(t => t.QuestionCategoryId);
            
            CreateTable(
                "dbo.AssessmentSchedulings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssessmentSchedulerName = c.String(),
                        AssessmentSetupId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        IsAnswered = c.Boolean(nullable: false),
                        HasCorrectivePlan = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AssessmentSetups", t => t.AssessmentSetupId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.AssessmentSetupId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        FirstName = c.String(),
                        DepartmentName = c.String(),
                        DepartmentId = c.Int(),
                        UserRole = c.Int(nullable: false),
                        strUserRole = c.String(),
                        ReliefEmail = c.String(),
                        SupervName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BatchUploads",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        BatchNo = c.String(),
                        FileName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionCategories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.CorrectivePlans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CorrectiveText = c.String(),
                        QuestionId = c.Int(nullable: false),
                        CorrectivePlanSetupId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CorrectivePlanSetups", t => t.CorrectivePlanSetupId, cascadeDelete: true)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId)
                .Index(t => t.CorrectivePlanSetupId);
            
            CreateTable(
                "dbo.CorrectivePlanSetups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssessmentSetupId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AssessmentSetups", t => t.AssessmentSetupId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.AssessmentSetupId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DepartmentName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CorrectivePlans", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.CorrectivePlans", "CorrectivePlanSetupId", "dbo.CorrectivePlanSetups");
            DropForeignKey("dbo.CorrectivePlanSetups", "UserId", "dbo.Users");
            DropForeignKey("dbo.CorrectivePlanSetups", "AssessmentSetupId", "dbo.AssessmentSetups");
            DropForeignKey("dbo.BatchUploads", "CategoryId", "dbo.QuestionCategories");
            DropForeignKey("dbo.AssessmentSchedulings", "UserId", "dbo.Users");
            DropForeignKey("dbo.AssessmentSchedulings", "AssessmentSetupId", "dbo.AssessmentSetups");
            DropForeignKey("dbo.AnswerEntities", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.Questions", "QuestionCategoryId", "dbo.QuestionCategories");
            DropForeignKey("dbo.AnswerEntities", "AssessmentRecordId", "dbo.AssessmentRecords");
            DropForeignKey("dbo.AssessmentRecords", "QuestionCategoryId", "dbo.QuestionCategories");
            DropForeignKey("dbo.AssessmentRecords", "AssessmentSetupId", "dbo.AssessmentSetups");
            DropIndex("dbo.CorrectivePlanSetups", new[] { "UserId" });
            DropIndex("dbo.CorrectivePlanSetups", new[] { "AssessmentSetupId" });
            DropIndex("dbo.CorrectivePlans", new[] { "CorrectivePlanSetupId" });
            DropIndex("dbo.CorrectivePlans", new[] { "QuestionId" });
            DropIndex("dbo.BatchUploads", new[] { "CategoryId" });
            DropIndex("dbo.AssessmentSchedulings", new[] { "UserId" });
            DropIndex("dbo.AssessmentSchedulings", new[] { "AssessmentSetupId" });
            DropIndex("dbo.Questions", new[] { "QuestionCategoryId" });
            DropIndex("dbo.AssessmentRecords", new[] { "AssessmentSetupId" });
            DropIndex("dbo.AssessmentRecords", new[] { "QuestionCategoryId" });
            DropIndex("dbo.AnswerEntities", new[] { "AssessmentRecordId" });
            DropIndex("dbo.AnswerEntities", new[] { "QuestionId" });
            DropTable("dbo.Departments");
            DropTable("dbo.CorrectivePlanSetups");
            DropTable("dbo.CorrectivePlans");
            DropTable("dbo.BatchUploads");
            DropTable("dbo.Users");
            DropTable("dbo.AssessmentSchedulings");
            DropTable("dbo.Questions");
            DropTable("dbo.QuestionCategories");
            DropTable("dbo.AssessmentSetups");
            DropTable("dbo.AssessmentRecords");
            DropTable("dbo.AnswerEntities");
        }
    }
}
