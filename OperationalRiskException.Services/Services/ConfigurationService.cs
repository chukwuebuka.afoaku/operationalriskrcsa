﻿using OperationalRiskException.Core.Enums;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Data.HRRepository;
using OperationalRiskException.Data.HRRepository.HrRepository;
using OperationalRiskException.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OperationalRiskException.Services.Utilities;
using NLog;

namespace OperationalRiskException.Services.Services
{
    public class ConfigurationService : IConfigurationService
    {
        string connstring = ConfigurationManager.ConnectionStrings["HRDBContext"].ConnectionString;
        private IUserService _userService;
        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();

        public ConfigurationService()
        {
            _userService = new UserService();
        }

      
        public IEnumerable<DepartmentVM> GetListOfDepartment(string Country="NGA", bool CheckHeadOffice=false)
        {  
            return SQLQuery(Country, CheckHeadOffice);
        }
        private IEnumerable<DepartmentVM> SQLQuery(string country, bool CheckHO)
        {
            string proc = ConfigurationManager.AppSettings["ProcedureDepartments"].ToString();
            var lists = new List<DepartmentVM>();
            using (SqlConnection sql = new SqlConnection(connstring))
            {
                try
                {
                    sql.Open();
                    SqlCommand cmd = new SqlCommand(proc, sql);
                    cmd.Parameters.Add(new SqlParameter("@Country", country));
                    cmd.Parameters.Add(new SqlParameter("@CheckHO", CheckHO));
                    cmd.CommandType = CommandType.StoredProcedure;
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var dept = new DepartmentVM
                        {
                            BankBranchID = reader["BankBranchID"].ToString(),
                            CountryName = reader["CountryName"].ToString(),
                            departmentName = reader["departmentName"].ToString(),
                            Department = reader["Department"].ToString(),
                            OfficeAddress = reader["OfficeAddress"].ToString(),
                            solID= reader["solID"].ToString(),


                        };
                        lists.Add(dept);
                    }
                }
                catch (Exception ex) { }
                finally
                {
                    sql.Close();
                }
            }

            return lists;
        }

        public List<EnumViewModel> GetAllUBACountriesExceptNG()
        {
            var lists = new List<EnumViewModel>();
            var enumList =Enum.GetValues(typeof(UBACountries)).Cast<UBACountries>();
            foreach(var item in enumList)
            {
                if (item != (int)UBACountries.NGA)
                {
                    var newItem = new EnumViewModel
                    {
                        Description=item.GetDescription(),
                        Value=(int)item,
                        Name=item.ToString()
                    };
                    lists.Add(newItem);
                }    
            }
            return lists;
        }

        public IEnumerable<HR_STAFF_USER> GetListOfUBA(string DepartmentSOLID)
        {
            string Department = DepartmentSOLID.Split('^')[0];
            string SOLID = DepartmentSOLID.Split('^')[1];
            return SQLQueryListUser(Department, SOLID);
        }
        private IEnumerable<HR_STAFF_USER> SQLQueryListUser( string Department, string solID)
        {
            string proc = ConfigurationManager.AppSettings["ProcedureALLSTAFFS"].ToString();
            var lists = new List<HR_STAFF_USER>();
            using (SqlConnection sql = new SqlConnection(connstring))
            {
                try
                {
                    sql.Open();
                    SqlCommand cmd = new SqlCommand(proc, sql);
                    cmd.Parameters.Add(new SqlParameter("@solID", solID));
                    cmd.Parameters.Add(new SqlParameter("@Department", Department));
                    cmd.CommandType = CommandType.StoredProcedure;
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var staff = new HR_STAFF_USER
                        {

                            departmentName = reader["departmentName"].ToString(),
                            Department = reader["Department"].ToString(),
                            OfficeAddress = reader["OfficeAddress"].ToString(),
                            emailAddress = reader["emailAddress"].ToString(),
                            LeaveStatus = reader["LeaveStatus"].ToString(),
                            ReliefEmail = reader["ReliefEmail"].ToString(),
                            username= reader["emailAddress"].ToString(),
                            id= long.Parse(reader["id"].ToString()),

                        };
                        lists.Add(staff);
                    }
                }
                catch (Exception ex) { }
                finally
                {
                    sql.Close();
                }
            }

            return lists;
        }

        public HR_STAFF_USER GetUserDetails(int id)
        {
            //if (!String.IsNullOrEmpty(username.Contains("@ubagroup.com") )
            //    username += "@ubagroup.com";
                string proc = ConfigurationManager.AppSettings["ProcedureUserDetails"].ToString();
            var user = new HR_STAFF_USER();
            using (SqlConnection sql = new SqlConnection(connstring))
            {
                try
                {
                    sql.Open();
                    SqlCommand cmd = new SqlCommand(proc, sql);
                    cmd.Parameters.Add(new SqlParameter("@id", id));
                     cmd.CommandType = CommandType.StoredProcedure;
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var staff = new HR_STAFF_USER
                        {

                            departmentName = reader["departmentName"].ToString(),
                            Department = reader["Department"].ToString(),
                            OfficeAddress = reader["OfficeAddress"].ToString(),
                            emailAddress = reader["emailAddress"].ToString(),
                            LeaveStatus = reader["LeaveStatus"].ToString(),
                            ReliefEmail = reader["ReliefEmail"].ToString(),
                            id=  long.Parse(reader["id"].ToString()),
                            firstName= reader["firstName"].ToString(),
                            middleName= reader["middleName"].ToString(),
                            Name= reader["Name"].ToString(),
                            
                        };
                        user = staff;
                    }
                }
                catch (Exception ex) { }
                finally
                {
                    sql.Close();
                }
            }

            return user;
        }

        public async Task<bool> AddChampion(HR_STAFF_USER user)
        {
            try
            {
                var userModel = new AddUserVM
                {
                   
                    DepartmentName = user.Department,
                    UserRole = UserRoleEnum.Champion,
                    Username = user.emailAddress,

                };
                var AddUser = await _userService.CreateUser(userModel);
                return AddUser;
            }
            catch(Exception ex)
            {
                _loggerInfo.Error(ex, "Create User:::: ");
                return false;
            }
           // throw new NotImplementedException();
        }
    }
}
