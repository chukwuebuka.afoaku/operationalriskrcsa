﻿using NLog;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Data.Context;
using OperationalRiskException.Data.GenericRepository.IRepository;
using OperationalRiskException.Data.GenericRepository.Repository;
using OperationalRiskException.Data.UnitOfWork;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Services
{
    public class QuestionService: IQuestionService
    {


        private IRepository<Question> _questionRepository;
        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        private IUnitOfWork _unitOfWork;
        public QuestionService()
        {
            _questionRepository = new Repository<Question>();
            _unitOfWork = new UnitOfWork(new OperationalRiskContext());
        }

        public async Task<bool> AddQuestion(QuestionVM model)
        {
            try
            {
                var entity = new Question
                {
                    Id = model.QuestionId,
                    QuestionText = model.QuestionText,
                    QuestionCategoryId = model.CategoryId
                };
                _questionRepository.Insert(entity);
                var status = await _questionRepository.SaveAsync();
                return status == 1;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "Error on Saving Question Category");
                return false;
            }

        }

        public async Task UpdateQuestion(QuestionVM model)
        {
            try
            {
                var entity = _questionRepository.GetSingleRecordFilter(x => x.Id == model.QuestionId && !x.IsDeleted);
                if (entity == null)
                    throw new Exception("Record does not exist");
                entity.QuestionText = model.QuestionText;
                entity.QuestionCategoryId = model.CategoryId;
                _questionRepository.Update(entity);
                var updateEn = await _questionRepository.SaveAsync();
                if (updateEn != 1)
                    throw new Exception("Failed to update");
            }
            catch (Exception ex)
            {

                _loggerInfo.Error(ex, "Error on Saving  Category");

            }
        }

        public async Task<QuestionVM> GetQuestion(int Id)
        {
            var entity = await _questionRepository.GetByIdAsync(Id);
            var qc = new QuestionVM
            {
                QuestionText = entity.QuestionText,
                CategoryId = (int)entity.QuestionCategoryId,
                QuestionId = entity.Id
            };
            return qc;
        }

        public async Task<int> DeleteQuestion(int questionId)
        {
            try
            {
                var entity = _questionRepository.GetSingleRecordFilter(x => x.Id == questionId && !x.IsDeleted);
                if (entity != null)
                {
                    _questionRepository.Delete(entity);
                    var deleteEn = await _questionRepository.SaveAsync();
                    return deleteEn;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                _unitOfWork.RollBack();
                throw new Exception("Invalid records");

            }
        }
        public IList<QuestionVM> GetAllQuestionDataTable(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount)
        {
            var searchBy = (model.search != null) ? model.search.value : null;
            var take = model.length;
            var skip = model.start;
            string sortBy = "";
            bool sortDir = true;

            if (model.order != null)
            {
                // in this example we just default sort on the 1st column
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }
            // search the dbase taking into consideration table sorting and paging
            var result = GetDataQuestionsFromDbase(searchBy, take, skip, sortBy, sortDir, out filteredResultsCount, out totalResultsCount);
            if (result == null)
            {
                result = new List<QuestionVM>();
                return result;
            }
            return result;
        }
        private List<QuestionVM> GetDataQuestionsFromDbase(string searchBy, int take, int skip, string sortBy, bool sortDir, out int filteredResultsCount, out int totalResultsCount)
        {
            //var role = Enum.Parse(typeof(UserRoleEnum), searchBy);
            var record = _questionRepository.GetAllIncluding(x => !x.IsDeleted, false, x => x.QuestionCategory);
            totalResultsCount = record.Count();
            if (!String.IsNullOrEmpty(searchBy))
                record = record.Where(x => x.QuestionText.ToLower().Contains(searchBy.ToLower()) ||
                x.QuestionCategory.CategoryName.ToLower().Contains(searchBy.ToLower())
                );
            //SortBy
            record = sortBy == "QuestionText" ? record.OrderBy<Question>(x => x.QuestionText, sortDir) :
                   sortBy == "CategoryName" ? record.OrderBy<Question>(x => x.QuestionCategory.CategoryName, sortDir) :
                  record.OrderByDescending(x => x.Id);
            filteredResultsCount = record.Count();
            var result = record.Skip(skip).Take(take).ToList();
            return result.Select(x => new QuestionVM
            {
                QuestionText = x.QuestionText,
                QuestionId = x.Id,
                CategoryId = x.QuestionCategoryId,
                CategoryName = x.QuestionCategory.CategoryName

            }).ToList();
        }

       
        public int ExcelUpload(QuestionVM question, Stream FileUpload, out int shetCount)
        {
            int sheet = 0;
            shetCount = 0;
            try
            {
                Guid guid = Guid.NewGuid();
                string BatchNo = guid.ToString();
                sheet = GetAllRecords(FileUpload, BatchNo, question.CategoryId, out int sheetCount);
                shetCount = sheetCount;
            }
            catch (Exception ex)
            {

            }
            return sheet;
        }
        private int GetAllRecords(Stream FileUpload, string batchNo, int categoryId, out int sheetCount)
        {
            int rowSave = 0;
            IDictionary<int, string> mapper = new Dictionary<int, string>();
            List<Question> records = new List<Question>();
            ISheet sheet;
            XSSFWorkbook hssfwb = new XSSFWorkbook(FileUpload);
            sheet = hssfwb.GetSheetAt(0);
            IRow headerRow = sheet.GetRow(0);
            int cellCount = headerRow.LastCellNum;
            sheetCount = sheet.LastRowNum;
            //getting the header
            for (int j = 0; j < cellCount; j++)
            {
                ICell cell = headerRow.GetCell(j);
                if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
                mapper.Add(j, cell.ToString());
                //headers.Add(cell.ToString());
            }
            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
            {
                var entity = new Question();
                IRow row = sheet.GetRow(i);
                if (row == null) continue;
                if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                for (int j = row.FirstCellNum; j < cellCount; j++)
                {
                    if (row.GetCell(j) != null)
                    {
                        entity.QuestionText = row.GetCell(j).ToString();
                    }
                }
                entity.BatchNo = batchNo;
                entity.QuestionCategoryId = categoryId;
                _questionRepository.Insert(entity);
                var questn = _questionRepository.Save();
                rowSave += questn;

            }


            return rowSave;
        }

    }
}
