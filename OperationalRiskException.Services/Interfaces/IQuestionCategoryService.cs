﻿using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace OperationalRiskException.Services.Interfaces
{
    public interface IQuestionCategoryService
    {
        Task<bool> AddQuestionCategory(QuestionCategoryVM model);
        Task<bool> UpdateQuestionCategory(QuestionCategoryVM model);
        int DeleteQuestionCategory(int Id);
        IEnumerable <QuestionCategoryVM> GetQuestionCat();
        IList<QuestionCategoryVM> GetAllQuestionCategoriesDataTable(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount);
        Task<QuestionCategoryVM> GetSingleQuestionCategory(int Id);
        IEnumerable<QuestionCategoryVM> GetAllQuestionCategories();
       

    }
}
