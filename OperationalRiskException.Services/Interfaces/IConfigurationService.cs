﻿using OperationalRiskException.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Interfaces
{
    public interface IConfigurationService
    {
        List<EnumViewModel> GetAllUBACountriesExceptNG();
        IEnumerable<DepartmentVM> GetListOfDepartment(string Country, bool CheckHeadOffice);

        IEnumerable<HR_STAFF_USER> GetListOfUBA(string DepartmentSOLID);
        HR_STAFF_USER GetUserDetails(int id);
        Task<bool> AddChampion(HR_STAFF_USER user);
       
    }
}
