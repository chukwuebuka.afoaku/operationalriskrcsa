﻿using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.AuthVM;
using OperationalRiskException.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OperationalRiskException.Core.Enums;
using OperationalRiskException.Core.DataTableModels;

namespace OperationalRiskException.Services.Interfaces
{
    public interface IUserService
    {
        Task<bool> UserExisted(int Id);
        Task<bool> ValidateUserOnADAsync(LoginViewModel loginModel);
        Task<bool> ValidateUserOnADAsyncMock(LoginViewModel loginModel);
        AuthenticationResponce TokenAuthenticateAsync(EntrustRequest _param);
        User CheckUserExistence(string username);
        Select2PagedResult GetSelect2PagedResult(string searchTerm, int pageSize, int pageNumber);
        IEnumerable<UserRoleEnum> GetUserROles();
        Task<bool> CreateUser(AddUserVM userVM);
        IList<User> GetAllUsersDataTable(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount);
        Task<bool> UpdateUser(AddUserVM updateUserVM, bool IsDeleted);
		Task<AddUserVM> GetUserRecord(int Id);
        Task<bool> CreateUserAdmin(string username);

    }
}
