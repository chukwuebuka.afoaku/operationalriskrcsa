﻿using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Helpers;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OperationalRiskException.Controllers
{
    public class QuestionController : Controller
    {
        // GET: Question
        private readonly IQuestionCategoryService  _questionCategory;
        private readonly IQuestionService _question;

        public QuestionController()
        {
            _questionCategory = new QuestionCategoryService();
            _question = new QuestionService();
        }
      

        //--Question Categore features--
        public ActionResult QCategoryPage()
        {
            
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> QCategoryPage(QuestionCategoryVM model)
        {
            if (ModelState.IsValid)
            {
				
               var checker= await _questionCategory.AddQuestionCategory(model);
                if (checker)
                {
                    TempData["CreateCat"] = "Question Category  created successfully";
                    return View();
                }
                else
                    TempData["Failed"] = "Question Category Name already existed";
            }


            // this did not work
            //model.CategoriesName = _questionCategory.GetQuestionCat()
            return View(model);
        }

		



		

        public JsonResult GetAllQuestionCategories(DataTableAjaxPostModel model)
        {
            int filteredResultsCount;
            int totalResultsCount;
            var result = _questionCategory.GetAllQuestionCategoriesDataTable(model, out filteredResultsCount, out totalResultsCount);
            return Json(new
            {
                model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data=result
            }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> UpdateQCategory(int Id)
        {
            var getQc =await _questionCategory.GetSingleQuestionCategory(Id);
            return View(getQc);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateQCategoryAsync(QuestionCategoryVM model)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
             status=  await _questionCategory.UpdateQuestionCategory(model);
            }
            return new JsonResult { Data = new { status } };
         
        }

        public async Task<ActionResult> DeleteQCategory(int Id)
        {
            var getQc = await _questionCategory.GetSingleQuestionCategory(Id);
            return View(getQc);
        }
        [HttpPost]
        public ActionResult DeleteQCategory(QuestionCategoryVM categoryVM)
        {
            
            var getQc =  _questionCategory.DeleteQuestionCategory(categoryVM.CategoryId);
           return new JsonResult { Data = new { status = getQc == 1} };
        }

		public ActionResult QuestionPage()


		{
			ViewBag.Categories = _questionCategory.GetAllQuestionCategories();
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> QuestionPage(QuestionVM model)
		{
			ViewBag.Categories = _questionCategory.GetAllQuestionCategories();

			if (ModelState.IsValid)
			{


				var checkSave = await _question.AddQuestion(model);
                if (checkSave)
                {
                    TempData["CreateQuest"] = "Question created successfully";
                    return View();
                }
                else
                {
                    TempData["FailedQuest"] = "Error in adding question, Pls, Contact the admin";

                    return View();
                }
				//TempData["CreateCat"] = "Question Category created successfully";
			}
			return View(model);
		}
		public JsonResult GetAllQuestions(DataTableAjaxPostModel model)
		{
			int filteredResultsCount;
			int totalResultsCount;
			var result = _question.GetAllQuestionDataTable(model, out filteredResultsCount, out totalResultsCount);
			return Json(new
			{
				model.draw,
				recordsTotal = totalResultsCount,
				recordsFiltered = filteredResultsCount,
				data = result
			}, JsonRequestBehavior.AllowGet);
		}
		public async Task<ActionResult> UpdateQuestion(int Id)
		{
			ViewBag.Categories = _questionCategory.GetAllQuestionCategories();

			var getQc = await _question.GetQuestion(Id);
			return View(getQc);
		}

		[HttpPost]
		public async Task<ActionResult> UpdateQuestion(QuestionVM model)
		{
			bool status = false;
			if (ModelState.IsValid)
			{
				try
				{
					await _question.UpdateQuestion(model);
					status = true;
					
				}
				catch (Exception ex) { }
			}
			return new JsonResult { Data = new { status = status } };

		}
		public async Task<ActionResult> DeleteQuestion(int Id)
		{
			var getQc = await _question.GetQuestion(Id);
			return View(getQc);
		}
		[HttpPost]
		public async Task<ActionResult> DeleteQuestion(QuestionVM question)
		{

			var getQc = await _question.DeleteQuestion(question.QuestionId);
			return new JsonResult { Data = new { status = getQc == 1 } };
		}
		public  ActionResult UploadQuestion()
		{
			ViewBag.Categories = _questionCategory.GetAllQuestionCategories();

			//var getQc = await _questionCategory.GetQuestion(Id);
			return View();
		}
		[HttpPost]
		public ActionResult UploadQuestion(QuestionVM question, HttpPostedFileBase FileUpload)
		{
			int TotalRows = 0;
			int SaveRows = 0;
			List<string> data = new List<string>();
			ViewBag.Categories = _questionCategory.GetAllQuestionCategories();
			if (FileUpload != null)
			{
				// tdata.ExecuteCommand("truncate table OtherCompanyAssets");  
				if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
				{
					string filename = FileUpload.FileName;
					string targetpath = Server.MapPath("~/Doc/");
					if (!Directory.Exists(targetpath))
					{
						Directory.CreateDirectory(targetpath);
					}
					FileUpload.SaveAs(targetpath + filename);
					string pathToExcelFile = targetpath + filename;
					string filePath = Server.MapPath(filename);
					FileInfo file = new FileInfo(pathToExcelFile);
					byte[] bin = System.IO.File.ReadAllBytes(pathToExcelFile);


					//create a new Excel package in a memorystream
					using (MemoryStream stream = new MemoryStream(bin))
					{
						if (filename.EndsWith(".xls") || filename.EndsWith(".xlsx"))
						{
							try
							{
								 SaveRows = _question.ExcelUpload(question, stream, out TotalRows);
								ViewBag.success = $"The TotalRows is {TotalRows-1}, Record Saves is {SaveRows} ";
								return View();
							}
							catch (Exception ex)
							{
								ViewBag.error = "";
							}


							//throw new Exception("File is not an excel sheet");
						}
					}
					//
					//deleting excel file from folder  
					if ((System.IO.File.Exists(pathToExcelFile)))
					{
						System.IO.File.Delete(pathToExcelFile);
					}
					ViewBag.Error = "";
					return View();
				}
				else
				{
					
				}
				return Json("error", JsonRequestBehavior.AllowGet);
			}
			else
			{
				return View();
			}
		}

	}
}