﻿using Newtonsoft.Json;
using OperationalRiskException.AuthorizationFilter;
using OperationalRiskException.Core.Enums;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OperationalRiskException.Controllers
{
    [CustomAuthorize(UserRoleEnum.Champion)]
    public class AssessmentPortalController : Controller
    {
        // GET: AssessmentPortal
        private IAssessmentPortalServices _assessmentPortalServices;

        public AssessmentPortalController()
        {
            _assessmentPortalServices = new AssessmentPortalServices();
        }

        public async Task<ActionResult> Dashboard()
        {
            var user = new CookieController().ReadFromCookie("UserRole");
            var deUser = JsonConvert.DeserializeObject<RoleViewModel>(user);
            string username = deUser.Username;

            var result = await _assessmentPortalServices.GetAllRecords(username);
            return View(result);
        }
        public ActionResult Records()
        {
            var totalScore = _assessmentPortalServices.GetAllScores();
            return View(totalScore);
        }
        public ActionResult CorrectivePlan(int AssSchId)
        {
            if (AssSchId == 0)
                return RedirectToAction("Login", "Account");
            var assSchId = new HttpCookie("AssSchId")
            {
                Value = AssSchId.ToString(),
                Expires = DateTime.Now.AddHours(24)
            };
            Response.Cookies.Add(assSchId);
            var correctList = _assessmentPortalServices.GetCorrectiveFormByUsername(AssSchId, Username());
            if (correctList.Count == 0)
                return RedirectToAction("");

            return View();
        }
    
    public ActionResult CorrectivePlanTest()
    {
            var assSchId = AssessmentSetUpId();
            if (assSchId == 0)
                return RedirectToAction("Login", "Account");
            var result =  _assessmentPortalServices.GetCorrectiveFormByUsername( assSchId, Username());
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);

    }
    public ActionResult TakeTest( int AssSchId=0)
        {
            if (AssSchId == 0)
                return RedirectToAction("Login","Account");



            var assSchId = new HttpCookie("AssSchId")
            {
                Value = AssSchId.ToString(),
                Expires = DateTime.Now.AddHours(24)
            };
            Response.Cookies.Add(assSchId);
     
            
            return View();
        }
        public async Task<ActionResult> GetAllQuestionsAsync()
        {
            //string username = new CookieController().ReadFromCookie("Username");
            var assSchId = AssessmentSetUpId();
            if (assSchId==0)
                return RedirectToAction("Login", "Account");
            var result = await _assessmentPortalServices.GetAllRecordAllQuestions(Username(), assSchId);
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> UploadAnswerAsync(HttpPostedFileBase fileBase )
        {
            var records = String.Empty;
            if (Request.Files.Count > 0)
            {
                string catId = Request["CategoryId"];

                //  Get all files from Request object  
                HttpFileCollectionBase Files = Request.Files;
                HttpPostedFileBase FileUpload = Files[0];
                int CateID = int.Parse(catId);
                // tdata.ExecuteCommand("truncate table OtherCompanyAssets");  
                if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    string filename = FileUpload.FileName;
                    string targetpath = Server.MapPath("~/Doc/");
                    if (!Directory.Exists(targetpath))
                    {
                        Directory.CreateDirectory(targetpath);
                    }
                    string absolutePath= "quiz" + DateTime.Now.ToString("MMddyyyyHHmmss") + filename;
                    string pathToExcelFile = targetpath + absolutePath;
                    FileUpload.SaveAs(pathToExcelFile);
                    var fileProperty = new FileUploadVM {
                        FilePath = absolutePath,
                        QuestionCategoryId=CateID
                    };
                  var Checker=await _assessmentPortalServices.SaveFile(fileProperty, Username(), AssessmentSetUpId());
                    records = Checker? pathToExcelFile : "failed";
                    
                }

            }
            return Json(new
            {
                data = records
            }, JsonRequestBehavior.AllowGet);


        }

        public async Task<ActionResult> SaveQuestionByCategoryAsync(SaveQuestionVM questionVM )
        {
            var scheduleId = AssessmentSetUpId();
         var check=   await _assessmentPortalServices.SaveAssessment(questionVM, Username(), scheduleId);
            //redirect if the answer is no
          
            return Json(new
            {
                data = check,
                scheduleId,
            }, JsonRequestBehavior.AllowGet);

        }

        public async Task<ActionResult> SaveQuestionByCorrectivePlanTest(SaveQuestionVM questionVM)
        {
            var scheduleId = AssessmentSetUpId();
            var check = await _assessmentPortalServices.SaveCorrectivePlan(questionVM, Username(), scheduleId);
            //redirect if the answer is no

            return Json(new
            {
                data = check,
                scheduleId,
            }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public async Task<JsonResult> GetAllResult(int QuestionCatId)
        {
            var records = await _assessmentPortalServices.GetAllSavedAssessment( Username(), QuestionCatId, AssessmentSetUpId());

            return Json(new
            {
                data = records
            }, JsonRequestBehavior.AllowGet);


        }
        [HttpGet]
        public async Task<JsonResult> GetAllUsersResult(int QuestionCatId)
        {
            var records = await _assessmentPortalServices.GetAllSavedAssessment(Username(), QuestionCatId, AssessmentSetUpId());

            return Json(new
            {
                data = records
            }, JsonRequestBehavior.AllowGet);


        }
        private int AssessmentSetUpId()
        {
            var aCookie = System.Web.HttpContext.Current.Request.Cookies["AssSchId"];
           return aCookie.Value != null ? int.Parse(aCookie.Value) : 0;
        }
        private string Username()
        {
            var user = new CookieController().ReadFromCookie("UserRole");
            var deUser = JsonConvert.DeserializeObject<RoleViewModel>(user);
            return deUser.Username;
        }


    }
}