﻿using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OperationalRiskException.Controllers
{
    public class AssessmentSetupController : Controller
    {
        // GET: AssessmentSetup
        // 

      
        private readonly IAssessmentSetupService _assessmentSetup;

        public AssessmentSetupController()
        {
            _assessmentSetup = new AssessmentSetupService(); 
        }

        public ActionResult Index()
        {

            var dateInit = new AssessmentSetupVM
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(1),                
            };
            return View(dateInit);
        }
        [HttpPost]
        public async Task<ActionResult> Index(AssessmentSetupVM model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedBy = "";
                var check = await _assessmentSetup.CreateAssessment(model);
                if (check)
                {
                    TempData["CreateCat"] = "Assessment created successfully";
               
                }
                else
                {
                    TempData["Failed"] = "Error in adding Assessment period, Please check if the period name is not on the system, or contact the admin";

                }
            }
             var dateInit = new AssessmentSetupVM
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(1)
            };
            return View(dateInit);
        }
        [HttpPost]
        public async Task<ActionResult> Update(AssessmentSetupVM model)
        {
           
            var result = await _assessmentSetup.UpdateAssessment(model);
            if (result)
            {
                TempData["CreateCat"] = " Updated successfully";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["Failed"] = "Error in updating Assessment period, Please check if the period name is not recurring on the system, or contact the admin";
                return View(model);
            }

        }
        public JsonResult GetAllCategories(string searchTerm, int pageSize, int pageNumber)
        {
            var result = _assessmentSetup.GetAllQuestionCategories(searchTerm, pageSize, pageNumber);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAssessmentConf(int id)
        {
            var check = _assessmentSetup.GetAssessmentSetup(id);
            return View("_UpdateAssessment", check);
        }
        public JsonResult GetQuestionCategories(int id)
        {
            var records = _assessmentSetup.GetQuestionCate(id);
            var reformattedRecords = records.Select(x => new { Text = x.Name,
            Id=x.Value.ToString()});
            return Json(new
            {
                data= reformattedRecords
            }, JsonRequestBehavior.AllowGet); ;
        }
        public ActionResult Update(int id)
        {
            var records = _assessmentSetup.GetAssessmentSetup(id);
            return View(records);
        }
        public ActionResult Delete(int id)
        {
            var records = _assessmentSetup.GetAssessmentSetup(id);
            return View(records);
        }
        [HttpPost]
        public async Task<ActionResult> Delete(AssessmentSetupVM model)
        {
          
            var result = await _assessmentSetup.DeleteAssessment(model);
            if (result)
            {
                TempData["CreateCat"] = " Updated successfully";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["Failed"] = "Error in updating Assessment period, Please check if the period name is not recurring on the system, or contact the admin";
                return View(model);
            }

        }
        public JsonResult GetAll(DataTableAjaxPostModel model)
        {
            var result = _assessmentSetup.GetAllDataTable(model, 
                out int filteredResultsCount, out int totalResultsCount);
            return Json(new
            {
                model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            }, JsonRequestBehavior.AllowGet);
        }


    }
}