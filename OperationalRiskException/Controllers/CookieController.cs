﻿using Newtonsoft.Json;
using OperationalRiskException.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OperationalRiskException.Controllers
{
    public class CookieController : Controller
    {
        public void AddToCookie(RoleViewModel roleView)
        {
            var sessionTimeout = Convert.ToDouble(ConfigurationManager.AppSettings["sessionTimeout"]);
            var userCookies = new System.Web.HttpCookie("UserRole")
            {
                Value = JsonConvert.SerializeObject(roleView),
                Expires = DateTime.Now.AddMinutes(sessionTimeout)
            };
            //userCookies.Secure = true;
            //userCookies.HttpOnly = true;
            System.Web.HttpContext.Current.Response.Cookies.Add(userCookies);
        }

        public string ReadFromCookie(string key)
        {
            var aCookie = System.Web.HttpContext.Current.Request.Cookies[key];
            var userRole = aCookie != null ? aCookie.Value : string.Empty;
            if (!string.IsNullOrWhiteSpace(userRole))
            {
                AddToCookie(JsonConvert.DeserializeObject<RoleViewModel>(userRole));
            }
            return userRole;
        }
    }
}