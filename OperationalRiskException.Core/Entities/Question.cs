﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
    public class Question : BaseEntity
    {
        public int Id { get; set; }
        public string QuestionText { get; set; }
        public string BatchNo { get; set; }
        public QuestionCategory QuestionCategory { get; set; }
        public int QuestionCategoryId { get; set; }
       
    }
}
