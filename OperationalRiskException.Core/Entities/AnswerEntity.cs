﻿using OperationalRiskException.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
  public  class AnswerEntity :BaseEntity
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public Question Question { get; set; }
        public AnswerEnum answerEnum { get; set; }
        public AssessmentRecord AssessmentRecord { get; set; }
        public int AssessmentRecordId { get; set; }

    }
}
