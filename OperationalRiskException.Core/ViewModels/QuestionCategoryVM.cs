﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
    public class QuestionCategoryVM 
    {
        public int CategoryId { get; set; }
		[Required(ErrorMessage = "Enter Question Categories")]
		public string CategoriesName { get; set; }
    }
}
