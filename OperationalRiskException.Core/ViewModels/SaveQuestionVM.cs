﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
   public class SaveQuestionVM
    {
        public bool IsCompleted { get; set; }
        public int QuestionCategoryId { get; set; }
        public List<ListQuestion> Questionlists { get; set; } 
    }

    public class ListQuestion
    {
        public string name { get; set; }
        public string value { get; set; }
    }
    public class FileUploadVM
    {
        public int QuestionCategoryId { get; set; }
        public string FilePath { get; set; }

    }
}
