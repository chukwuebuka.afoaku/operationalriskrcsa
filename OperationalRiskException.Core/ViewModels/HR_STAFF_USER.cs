﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
    public class HR_STAFF_USER
    {
        
            public long id { get; set; }
            public string employeeNumber { get; set; }
            public string surname { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string username { get; set; }
            public string solID { get; set; }
            public string emailAddress { get; set; }
            public string directorateName { get; set; }
            public string divisionName { get; set; }
            public string sDivisionName { get; set; }
            public string departmentName { get; set; }
            public string departmentHeadName { get; set; }
            public string countryName { get; set; }
        
            public string Name { get; set; }
            public string MobileNumber { get; set; }
            public string StaffID { get; set; }
            public string AccountNumber { get; set; }
            public string Country { get; set; }
            public string ParentCode { get; set; }
            public string Department { get; set; }
            public string OfficeAddress { get; set; }
            public string BankName { get; set; }
      
            public string EmployeeID { get; set; }
            public string Sex { get; set; }
       
            public string SupervName { get; set; }

            public string LeaveStatus { get; set; }
            public string ReliefName { get; set; }
            public string ReliefEmail { get; set; }
            public string Country2 { get; set; }
            public DateTime DateAdded { get; set; }
        public string CreatedBy { get; set; }
    }
    }

