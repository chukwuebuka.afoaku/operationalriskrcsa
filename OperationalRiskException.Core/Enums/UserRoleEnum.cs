﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Enums
{
    public enum UserRoleEnum
    {
        
        Admin=1,
        OperationalRisk,
        Deactivated,
        Champion
    }
}
