﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.AuthVM
{
    public class EntrustRequest
    {
        public string requesterId { get; set; }
        public string requesterIp { get; set; }
        public string response { get; set; }
        public string userGroup { get; set; }
        public string username { get; set; }
    }
}
