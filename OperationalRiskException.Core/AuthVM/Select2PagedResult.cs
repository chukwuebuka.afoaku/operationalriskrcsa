﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.AuthVM
{
    public class Select2PagedResult
    {
        public int Total { get; set; }
        public List<Select2OptionModel> Results { get; set; }
    }
}
